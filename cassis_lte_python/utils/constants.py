import astropy.constants.codata2014 as const  # h,k_B,c # SI units
import numpy as np
from matplotlib.pyplot import get_cmap
"""
A few useful constants
"""


C_LIGHT = const.c.value  # speed of light [m/s]
K_B = const.k_B.value  # Boltzman's constant [J/K]
H = const.h.value

FWHM2SIGMA = 1. / (2. * np.sqrt(2. * np.log(2.)))  # to convert the FWHM of a Gaussian to sigma

UNITS = {'flux': ['Jy/beam', 'Jy beam-1', 'beam-1 Jy'],
         'frequency': ['GHz', 'MHz'],
         'wavelength': ['nm', 'micron', 'um', 'A', 'angstrom']}
UNITS['xaxis'] = UNITS['frequency'] + UNITS['wavelength']
UNITS['yaxis'] = UNITS['flux'] + ['K', 'Kelvin', 'Jy']
# TODO : complete these lists

TEL_DIAM = {'iram': 30.,
            'apex': 12.,
            'jcmt': 15.,
            'gbt': 100.,
            'alma_400m': 400.,
            'alma_170m': 170.}

CPT_COLORS = ['blue', 'magenta', 'cyan', 'green', 'darkorange', 'darkviolet']
COLOR_RESIDUAL = 'moccasin'  # 'lightskyblue'

# CPT_COLORS = ['blue', 'orange', 'purple', 'brown', 'pink']
# CPT_COLORS = ['blue', 'green', 'mediumorchid']
# CPT_COLORS = [
#     'blue', 'dodgerblue', 'deepskyblue',
#     'orange',
#     'gold',
#     # 'yellow',
#     'green',
#     'purple', 'mediumorchid', 'pink']
TAB = get_cmap('tab20')(np.linspace(0, 1, 20))
PLOT_COLORS = np.concatenate([TAB[:][::2], TAB[:][1::2]])
# TAB = get_cmap('Paired')(np.linspace(0, 1, 12))
# PLOT_COLORS = np.concatenate([TAB[:][1::2], TAB[:][::2]])
# PLOT_LINESTYLES = ['-', '--']
PLOT_LINESTYLES = ['-', ':']
